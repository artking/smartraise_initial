import * as React from 'react';
import PropTypes from 'prop-types';
import cssModules from 'react-css-modules';
import Header from '../components/Header';
//const styles = require('../style/index.scss');
import styles from '../style/index.scss';

@cssModules(styles)
export default class App extends React.Component<any, any> {
  static propTypes = {
    children: PropTypes.any.isRequired,
    styles: PropTypes.object
  };

  render() {
    const { children, styles } = this.props;

    return (
      <div className={styles.container}>
        <Header />

        {children}
      </div>
    );
  }
}
